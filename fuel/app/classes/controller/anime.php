<?php
class Controller_Anime extends Controller_Template{
	public function action_detail($id = null){
		if($id == null){
			Session::set_flash('error', 'Maaf anda tidak memiliki hak akses');
			Response::redirect('page/anime_list');
		}
		if(!$anime = Model_Anime::find($id)){
			Session::set_flash('error', 'Maaf anime yang anda cari tidak tersedia');
			Response::redirect('anime');
		}
		$data = array(
			'anime'   => $anime,
			'user'    => Model_User::get_profile_fields(),
			'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
		);
		$this->template->title = $anime->name;
		$this->template->body  = View::forge('website/detail', $data);
	}

	public function action_news($id = null){
		if($id == null){
			Session::set_flash('error', 'Maaf anda tidak memiliki hak akses');
			Response::redirect('');
		}
		if(!$news = Model_News::find($id)){
			Session::set_flash('error', 'Maaf news yang anda cari tidak tersedia');
			Response::redirect('page/news_list');
		}
		$data = array(
			'news'   => $news,
			'user'    => Model_User::get_profile_fields(),
			'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
		);
		$this->template->title = $news->title;
		$this->template->body  = View::forge('website/news', $data);
	}

	public function action_genre($genre = null){
		if($genre == null){
			Session::set_flash('error', 'Maaf anda tidak memiliki hak akses');
			Response::redirect('page/genre_list');
		}
		$anime = \DB::query("SELECT * FROM animelists WHERE concat(',',genre,',') LIKE '%,{$genre},%'")->execute()->as_array();
		if(!count($anime)){
			Session::set_flash('error', 'Maaf genre yang anda cari tidak ada');
			Response::redirect('page/genre_list');
		}
		$data = array(
			'animes'  => $anime,
			'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
			'title'   => "Genre ".$genre,
		);
		$this->template->title = "Genre : ".$genre;
		$this->template->body  = View::forge('website/genre', $data);
	}
}
?>
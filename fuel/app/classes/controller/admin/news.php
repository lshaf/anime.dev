<?php
class Controller_Admin_News extends Controller_Admin{
	var $menu = "news";

	public function before(){
		parent::before();

		if(!Auth::has_access($this->menu.'.read')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman '.$this->menu);
			Response::redirect_back('admin');
		}
	}

	public function action_index(){
		$data = array(
			'news' => Model_News::find('all'),
			'user' => Model_User::get_profile_fields(),
		);

		$body['admin_body'] = View::forge('admin/news/index', $data);

		$this->template->title = "Admin";
		$this->template->body  = View::forge('admin/index', $body);
	}

	public function action_create(){
		if(!Auth::has_access($this->menu.'.create')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman membuat '.$this->menu);
			Response::redirect('admin/'.$this->menu);
		}

		if (Input::method() == 'POST'){
			$val = Model_News::validate('create');
			
			if ($val->run()){
				$data = Input::post();
				$data['pic'] = "";
				$send = true;
				list(, $data['user_id']) = Auth::get_user_id();
				if($_FILES['pic']['tmp_name'] != ""){
					if($tmp_data = Model_Anime::upload_pic($_FILES['pic'])){
						$data['pic'] = $tmp_data->links->image_link;
					}else{
						$send = false;
					}
				}
				$create = Model_News::forge($data);

				if($send){
					if ($create and $create->save()){
						Session::set_flash('success', 'News berhasil ditambahkan');
						Response::redirect('admin/news');
					}else{
						Session::set_flash('error', 'Maaf ada kesalahan ketika menyimpan');
					}
				}else{
					Session::set_flash('error', 'Maaf file yang anda upload tidak support');
				}
			}else{
				Session::set_flash('error', $val->error());
			}
		}

		$body['admin_body'] = View::forge('admin/news/form');

		$this->template->title = "Tambah News";
		$this->template->body = View::forge('admin/index', $body);
	}

	public function action_edit($id = null){
		if(!Auth::has_access($this->menu.'.update')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman merubah '.$this->menu);
			Response::redirect('/admin/'.$this->menu);
		}

		is_null($id) and Response::redirect('admin/news');

		if ( ! $news = Model_News::find($id)){
			Session::set_flash('error', 'Could not find news #'.$id);
			Response::redirect('admin/news/');
		}

		$val = Model_News::validate('edit');

		if ($val->run()){
			$send = true;
			if($_FILES['pic']['tmp_name'] != ""){
				if($tmp_data = Model_Anime::upload_pic($_FILES['pic'])){
					$news->pic = $tmp_data->links->image_link;
				}else{
					$send = false;
				}
			}
			if($send){
				$news->title   = Input::post('title');
				$news->detail = Input::post('detail');
				if($news->save()){
					Session::set_flash('success', 'Data news telah berubah');
					Response::redirect('admin/news');
				}else{
					Session::set_flash('error', 'Kesalahan ketika menyimpan data');
				}
			}else{
				Session::set_flash('error', 'Maaf file yang anda upload tidak support');
			}
		}else{
			if (Input::method() == 'POST'){
				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('news', $news, false);
		}

		$data['admin_body'] = View::forge('admin/news/form');

		$this->template->title = "Rubah News";
		$this->template->body = View::forge('admin/index', $data);
	}

	public function action_delete($id = null){
		if(!Auth::has_access($this->menu.'.delete')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman menghapus '.$this->menu);
			Response::redirect_back('/admin/'.$this->menu);
		}

		is_null($id) and Response::redirect('admin/news');

		if ($news = Model_News::find($id)){
			$news->delete();
			Session::set_flash('success', 'Deleted news #'.$id);
		}else{
			Session::set_flash('error', 'Could not delete news #'.$id);
		}

		Response::redirect('admin/news');
	}
}
?>
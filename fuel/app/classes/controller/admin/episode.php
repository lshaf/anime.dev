<?php
class Controller_Admin_Episode extends Controller_Admin{
	var $menu = "episode";

	public function before(){
		parent::before();

		if(!Auth::has_access($this->menu.'.read')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman '.$this->menu);
			Response::redirect_back('admin');
		}
	}

	public function action_index(){
		if(!Input::get('anime')){
			$search_data = array();
		}else{
			$search_data = array(
				'where' => array(
					array('anime_id', Input::get('anime')),
				),
			);
		}
		$data = array(
			'episodes'   => Model_Episode::find('all', $search_data),
			'animelists' => Model_Anime::find('all', array('order_by' => array('name' => 'ASC'))),
		);

		$body['admin_body'] = View::forge('admin/episode/index', $data);

		$this->template->title = "Admin";
		$this->template->body  = View::forge('admin/index', $body);
	}

	public function action_create(){
		if(!Auth::has_access($this->menu.'.create')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman membuat '.$this->menu);
			Response::redirect('admin/'.$this->menu);
		}

		if (Input::method() == 'POST'){
			$val = Model_Episode::validate('create');
			
			if ($val->run()){
				$data = Input::post();
				$create = Model_Episode::forge($data);

				if ($create and $create->save()){
					Session::set_flash('success', 'Anime berhasil ditambahkan');
					Response::redirect('admin/episode');
				}else{
					Session::set_flash('error', 'Maaf ada kesalahan ketika menyimpan');
				}
			}else{
				Session::set_flash('error', $val->error());
			}
		}

		$data = array(
			'animelists' => Model_Anime::find('all', array('order_by' => array('name' => 'ASC'))),
		);

		$body['admin_body'] = View::forge('admin/episode/form', $data);

		$this->template->title = "Tambah Episode";
		$this->template->body = View::forge('admin/index', $body);
	}

	public function action_edit($id = null){
		if(!Auth::has_access($this->menu.'.update')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman merubah '.$this->menu);
			Response::redirect('/admin/'.$this->menu);
		}

		is_null($id) and Response::redirect('admin/episode');

		if ( ! $episode = Model_Episode::find($id)){
			Session::set_flash('error', 'Could not find episode #'.$id);
			Response::redirect('admin/episode');
		}

		$val = Model_Episode::validate('edit');

		if ($val->run()){
			$episode->anime_id = Input::post('anime_id');
			$episode->title    = Input::post('title');
			$episode->link1    = Input::post('link1');
			$episode->link2    = Input::post('link2');
			$episode->link3    = Input::post('link3');
			if($episode->save()){
				Session::set_flash('success', 'Data episode telah berubah');
				Response::redirect('admin/episode');
			}else{
				Session::set_flash('error', 'Kesalahan ketika menyimpan data');
			}
		}else{
			if (Input::method() == 'POST'){
				Session::set_flash('error', $val->error());
			}
		}

		$data = array(
			'episode' => $episode,
			'animelists' => Model_Anime::find('all', array('order_by' => array('name' => 'ASC'))),
		);

		$data['admin_body'] = View::forge('admin/episode/form', $data);

		$this->template->title = "Rubah Episode";
		$this->template->body = View::forge('admin/index', $data);
	}

	public function action_delete($id = null){
		if(!Auth::has_access($this->menu.'.delete')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman menghapus '.$this->menu);
			Response::redirect_back('/admin/'.$this->menu);
		}

		is_null($id) and Response::redirect('admin/episode');

		if ($anime = Model_Episode::find($id)){
			$anime->delete();
			Session::set_flash('success', 'Deleted episode #'.$id);
		}else{
			Session::set_flash('error', 'Could not delete episode #'.$id);
		}

		Response::redirect('admin/episode');
	}
}
?>
<?php
class Controller_Admin_Anime extends Controller_Admin{
	var $menu = "anime";

	public function before(){
		parent::before();

		if(!Auth::has_access($this->menu.'.read')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman '.$this->menu);
			Response::redirect_back('admin');
		}
	}


	public function action_index(){
		$data = array(
			'animelists' => Model_Anime::find('all'),
			'user'       => Model_User::get_profile_fields(),
		);

		$body['admin_body'] = View::forge('admin/anime/index', $data);

		$this->template->title = "Admin";
		$this->template->body  = View::forge('admin/index', $body);
	}

	public function action_create(){
		if(!Auth::has_access($this->menu.'.create')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman membuat '.$this->menu);
			Response::redirect('admin/'.$this->menu);
		}

		if (Input::method() == 'POST'){
			$val = Model_Anime::validate('create');
			
			if ($val->run()){
				$data = Input::post();
				$data['pic'] = "";
				$send = true;
				list(, $data['user_id']) = Auth::get_user_id();
				if($_FILES['pic']['tmp_name'] != ""){
					if($tmp_data = Model_Anime::upload_pic($_FILES['pic'])){
						$data['pic'] = $tmp_data->links->image_link;
					}else{
						$send = false;
					}
				}
				$create = Model_Anime::forge($data);

				if($send){
					if ($create and $create->save()){
						Session::set_flash('success', 'Anime berhasil ditambahkan');
						Response::redirect('admin/anime');
					}else{
						Session::set_flash('error', 'Maaf ada kesalahan ketika menyimpan');
					}
				}else{
					Session::set_flash('error', 'Maaf file yang anda upload tidak support');
				}
			}else{
				Session::set_flash('error', $val->error());
			}
		}

		$body['admin_body'] = View::forge('admin/anime/form');

		$this->template->title = "Tambah Anime";
		$this->template->body = View::forge('admin/index', $body);
	}

	public function action_edit($id = null){
		if(!Auth::has_access($this->menu.'.update')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman merubah '.$this->menu);
			Response::redirect('/admin/'.$this->menu);
		}

		is_null($id) and Response::redirect('admin/anime');

		if ( ! $anime = Model_Anime::find($id)){
			Session::set_flash('error', 'Could not find anime #'.$id);
			Response::redirect('admin/anime/');
		}

		$val = Model_Anime::validate('edit');

		if ($val->run()){
			$send = true;
			if($_FILES['pic']['tmp_name'] != ""){
				if($tmp_data = Model_Anime::upload_pic($_FILES['pic'])){
					$anime->pic = $tmp_data->links->image_link;
				}else{
					$send = false;
				}
			}
			if($send){
				$anime->name   = Input::post('name');
				$anime->genre  = Input::post('genre');
				$anime->detail = Input::post('detail');
				if($anime->save()){
					Session::set_flash('success', 'Data anime telah berubah');
					Response::redirect('admin/anime');
				}else{
					Session::set_flash('error', 'Kesalahan ketika menyimpan data');
				}
			}else{
				Session::set_flash('error', 'Maaf file yang anda upload tidak support');
			}
		}else{
			if (Input::method() == 'POST'){
				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('anime', $anime, false);
		}

		$data['admin_body'] = View::forge('admin/anime/form');

		$this->template->title = "Rubah Anime";
		$this->template->body = View::forge('admin/index', $data);
	}

	public function action_delete($id = null){
		if(!Auth::has_access($this->menu.'.delete')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman menghapus '.$this->menu);
			Response::redirect_back('/admin/'.$this->menu);
		}

		is_null($id) and Response::redirect('admin/anime');

		if ($anime = Model_Anime::find($id)){
			$anime->delete();
			Session::set_flash('success', 'Deleted anime #'.$id);
		}else{
			Session::set_flash('error', 'Could not delete anime #'.$id);
		}

		Response::redirect('admin/anime');
	}
}
?>
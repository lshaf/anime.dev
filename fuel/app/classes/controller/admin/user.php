<?php
class Controller_Admin_User extends Controller_Admin{
	var $menu = "user";

	public function before(){
		parent::before();

		if(!Auth::has_access($this->menu.'.read')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman '.$this->menu);
			Response::redirect_back('admin');
		}
	}

	public function action_index(){
		$users   = Model_User::find('all');
		$profile = Model_User::get_profile_fields();

		$data = array(
			'users'   => $users,
			'profile' => $profile,
		);
		$body['admin_body'] = View::forge('admin/user/index', $data);
		$this->template->title = "Admin";
		$this->template->body  = View::forge('admin/index', $body);
	}

	public function action_create(){
		if(!Auth::has_access($this->menu.'.create')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman membuat '.$this->menu);
			Response::redirect('admin/'.$this->menu);
		}

		if (Input::method() == 'POST'){
			$val = Model_User::validate('create');
			
			if ($val->run()){
				try {
					$user = Auth::create_user(
						Input::post('username'),
						Input::post('password'),
						Input::post('email'),
						Input::post('group'),
						array(
							'fullname' => Input::post('name'),
						)
					);
					if ($user){
						Session::set_flash('success', 'Pengguna berhasil ditambahkan');
						Response::redirect('admin/user');
					}
				} catch (SimpleUserUpdateException $e) {
					Session::set_flash('error', 'Tidak dapat disimpan karena '.$e->getMessage());
				}
			}else{
				Session::set_flash('error', $val->error());
			}
		}

		//load all group
		\Config::load('simpleauth');
		$groups = \Config::get('simpleauth.groups');
		$data_group = array();

		foreach($groups as $group) {
			$data_group[] = $group['name'];
		}
		//end load

		$this->template->set_global('groups', $data_group, false);

		$data['admin_body'] = View::forge('admin/user/form');

		$this->template->title = "Add User";
		$this->template->body = View::forge('admin/index', $data);
	}

	public function action_edit($id = null){
		if(!Auth::has_access($this->menu.'.update')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman merubah '.$this->menu);
			Response::redirect('/admin/'.$this->menu);
		}

		is_null($id) and Response::redirect('admin/user');

		if ( ! $user = Model_User::find($id)){
			Session::set_flash('error', 'Could not find user #'.$id);
			Response::redirect('setting/user');
		}

		$val = Model_User::validate('edit');

		if ($val->run()){
			try {
				$data_update = array(
					'email'    => Input::post('email'),
					'group'	   => Input::post('group'),
					'fullname' => Input::post('name'),
			    );
			    if(Input::post('password') != ""){
			    	$data_update['password'] = Input::post('password');
			    	$data_update['old_password'] = Input::post('cpassword');
			    }
				$backend = Auth::update_user($data_update, $user->username);
				if ($backend){
					Session::set_flash('success', 'Pengguna berhasil dirubah');
					Response::redirect('admin/user');
				}
			} catch (SimpleUserUpdateException $e) {
				Session::set_flash('error', 'Could not save because '.$e->getMessage());
			} catch (SimpleUserWrongPassword $e){
				Session::set_flash('error', 'Could not save because '.$e->getMessage());
			}
		}else{
			if (Input::method() == 'POST'){
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->set_global('user', $user, false);
		$this->template->set_global('profile', Model_User::get_profile_fields($user->username), false);

		//load all group
		\Config::load('simpleauth');
		$groups = \Config::get('simpleauth.groups');
		$data_group = array();

		foreach($groups as $group) {
			$data_group[] = $group['name'];
		}
		//end load

		$this->template->set_global('groups', $data_group, false);

		$data['admin_body'] = View::forge('admin/user/form');

		$this->template->title = "Rubah Pengguna";
		$this->template->body = View::forge('admin/index', $data);
	}

	public function action_delete($id = null){
		if(!Auth::has_access($this->menu.'.delete')){
			Session::set_flash('error', 'Anda tidak memiliki hak akses ke halaman menghapus '.$this->menu);
			Response::redirect_back('/admin/'.$this->menu);
		}
		
		is_null($id) and Response::redirect('admin/user');

		if ($user = Model_User::find($id)){
			Auth::delete_user($user->username);
			Session::set_flash('success', 'Pengguna berhasil dihapus');
		}else{
			Session::set_flash('error', 'Could not delete user #'.$id);
		}

		Response::redirect('admin/user');

	}
}
?>
<?php
	class Controller_Admin_Login extends Controller_Template{
		public function action_index(){
			if(Auth::check()){
				Response::redirect('/admin/');
			}

			$this->template->title = "Login";
			$this->template->body  = View::forge('admin/login');
		}

		public function action_check(){
			if(Input::method() == "POST"){
				if(Auth::login(Input::post('email'), Input::post('pass'))){
					Response::redirect('/admin/');
				}else{
					Session::set_flash('error', 'Username or Password is wrong');
					Response::redirect('/admin/login');
				}
			}else{
				Response::redirect('/');
			}
		}
	}
?>
<?php
	class Controller_Page extends Controller_Template{
		public function action_index(){
			$data = array(
				'animelists' => Model_Anime::find('all', array(
					'order_by' => array('id' => 'DESC'),
					'limit' => 5,
				)),
				'updatelists' => Model_Episode::find('all', array(
					'order_by' => array('id' => 'DESC'),
					'limit' => 12,
				)),
				'news' => Model_News::find('all', array(
					'order_by' => array('id' => 'DESC'),
					'limit' => 5,
				))
			);
			$this->template->title = "Home";
			$this->template->body  = View::forge('website/index', $data);
		}

		public function action_problem(){
			$data = array(
				'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
			);
			$this->template->title = "Broken Link?";
			$this->template->body  = View::forge('website/forum', $data);
		}

		public function action_book(){
			$data = array(
				'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
			);
			$this->template->title = "Guest Book";
			$this->template->body  = View::forge('website/forum', $data);
		}

		public function action_anime_list(){
			$data = array(
				'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
				'animes'  => Model_Anime::find('all', array(
					'order_by' => array('name' => 'ASC')
				)),
			);
			$this->template->title = "Anime List";
			$this->template->body  = View::forge('website/anime_list', $data);
		}

		public function action_genre_list(){
			$data = array(
				'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
				'genres'   => Model_Anime::genre_anime(),
			);
			$this->template->title = "Genre List";
			$this->template->body  = View::forge('website/genre_list', $data);
		}

		public function action_update_list(){
			$data = array(
				'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
				'updates' => Model_Episode::find('all', array(
					'order_by' => array('id' => 'DESC'),
					'limit' => 30,
				)),
			);
			$this->template->title = "Update List";
			$this->template->body  = View::forge('website/update_list', $data);
		}

		public function action_news_list(){
			$data = array(
				'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
				'news'  => Model_News::find('all', array(
					'order_by' => array('id' => 'DESC')
				)),
			);
			$this->template->title = "News List";
			$this->template->body  = View::forge('website/news_list', $data);
		}

		public function action_about(){
			$data = array(
				'sidebar' => View::forge('website/sidebar', Model_Anime::sidebar_cont()),
			);
			$this->template->title = "Guest Book";
			$this->template->body  = View::forge('website/about', $data);
		}
	}
?>
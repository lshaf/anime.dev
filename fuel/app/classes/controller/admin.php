<?php
	class Controller_Admin extends Controller_Template{

		public function before(){
			parent::before();

			if(!Auth::check()){
				Session::set_flash('error', 'Login dulu');
				Response::redirect('admin/login');
			}
		}

		public function action_index(){
			$data['admin_body'] = View::forge('admin/dashboard');

			$this->template->title = "Admin";
			$this->template->body  = View::forge('admin/index', $data);
		}

		public function action_logout(){
			Auth::logout();
			Response::redirect('/');
		}
	}
?>
<?php
use Orm\Model;

class Model_Episode extends Model{
	protected static $_properties = array(
		'id',
		'anime_id',
		'title',
		'link1',
		'link2',
		'link3',
		'meta',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);

	protected static $_belongs_to = array(
		'anime' => array(
			'key_from' => 'anime_id',
			'model_to' => 'Model_Anime',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		)
	);
	
	protected static $_table_name = 'episodelists';

	public static function validate($factory){
		$val = Validation::forge($factory);
		$val->add_field('anime_id', 'Anime', 'required|valid_string[numeric]');
		$val->add_field('link1', 'Primary Link', 'required|max_length[255]|valid_url');
		$val->add_field('title', 'Title', 'required|max_length[255]');

		return $val;
	}
}
?>
<?php
use Orm\Model;

class Model_User extends Model{
	protected static $_properties = array(
		'id',
		'username',
		'password',
		'email',
		'group',
		'profile_fields',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);

	protected static $_has_many = array(
		'anime' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Anime',
			'key_to' => 'user_id',
			'cascade_save' => true,
			'cascade_delete' => true
		),
		'news' => array(
			'key_from' => 'id',
			'model_to' => 'Model_News',
			'key_to' => 'user_id',
			'cascade_save' => true,
			'cascade_delete' => true
		)
	);
	
	protected static $_table_name = 'users';

	public static function validate($factory){
		$val = Validation::forge($factory);
		if($factory != 'edit'){
			$val->add_field('password', 'Password', 'required|max_length[255]');
			$val->add_field('username', 'Username', 'required|max_length[50]');
		}
		$val->add_field('group', 'Group', 'required|valid_string[numeric]');
		$val->add_field('email', 'Email', 'required|valid_email|max_length[255]');
		$val->add_field('name', 'Name', 'required|max_length[255]');

		return $val;
	}

	public static function get_profile_fields($user = null, $field = null, $default = null){
		if(is_null($user)){
			$data = self::find('all');
		}else{
			$data = self::find_by_username($user);
		}

		if(is_null($user)){
			$return = array();

			foreach ($data as $detail) {
				$return[$detail->username] = @unserialize($detail->profile_fields);
			}

			return $return;
		}else{
			if (isset($data->profile_fields)){
				is_array($data->profile_fields) or $data->profile_fields = @unserialize($data->profile_fields);
			}else{
				$data->profile_fields = array();
			}

			return is_null($field) ? $data->profile_fields : \Arr::get($data->profile_fields, $field, $default);
		}
	}

}
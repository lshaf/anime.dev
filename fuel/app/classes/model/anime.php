<?php
use Orm\Model;
use Guzzle\Http\Client;

class Model_Anime extends Model{
	protected static $_properties = array(
		'id',
		'name',
		'user_id',
		'pic',
		'genre',
		'detail',
		'meta',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);

	protected static $_has_many = array(
		'episode' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Episode',
			'key_to' => 'anime_id',
			'cascade_save' => true,
			'cascade_delete' => true
		)
	);

	protected static $_belongs_to = array(
		'user' => array(
			'key_from' => 'user_id',
			'model_to' => 'Model_User',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		)
	);
	
	protected static $_table_name = 'animelists';

	public static function validate($factory){
		$val = Validation::forge($factory);
		$val->add_field('name', 'Title', 'required|max_length[255]');
		$val->add_field('genre', 'Genre', 'required');
		$val->add_field('detail', 'Detail', 'required');

		return $val;
	}

	public static function upload_pic($form){
		$client = new Client();
		$path = $form['tmp_name'];
		$fnam = $form['name'];
		$fext = explode(".", $fnam);
		$aext = array('jpg', 'png', 'jpeg', 'gif');
		$dest = DOCROOT."/assets/img/{$fnam}";
		if(in_array(strtolower($fext[count($fext)-1]), $aext, true)){
			move_uploaded_file($path, $dest);
		}else{
			unlink($path);
			return false;
		}
		$key = '057CDIRS31619318017147c3ba5a52b61d7aef31';
		$request = $client->post('https://post.imageshack.us/upload_api.php', array(), array(
			'key' => $key,
			'format' => 'json',
			'fileupload' => '@'.$dest,
		));
		$response = $request->send()->getBody();
		unlink($dest);
		return json_decode($response);
	}

	public static function genre_anime($limit = null){
		$query  = DB::select('genre')->from('animelists');
		$genres = $query->as_object()->execute();
		$result = array();
		foreach ($genres as $genre) {
			$data_exp = explode(",", $genre->genre);
			for ($i = 0; $i < count($data_exp); $i++){ 
				if(!is_null($limit)){
					if(count($result) == $limit) break;
				}
				if(!in_array($data_exp[$i], $result)){
					$result[] = $data_exp[$i];
				}
			}
			if(!is_null($limit)){
				if(count($result) == $limit) break;
			}
		}

		return $result;
	}

	public static function sidebar_cont(){
		return array(
			'animes'   => Model_Anime::find('all', array(
				'order_by' => array('id' => 'DESC'),
				'limit' => 7,
			)),
			'episodes' => Model_Episode::find('all', array(
				'order_by' => array('id' => 'DESC'),
				'limit' => 7,
			)),
			'news' => Model_News::find('all', array(
				'order_by' => array('id' => 'DESC'),
				'limit' => 7,
			)),
			'genres'   => self::genre_anime(7),
		);
	}
}
?>
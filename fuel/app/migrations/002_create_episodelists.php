<?php

namespace Fuel\Migrations;

class Create_episodelists {

  public function up() {
    \DB::query("CREATE  TABLE IF NOT EXISTS `episodelists` (
          `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
          `anime_id` INT UNSIGNED NOT NULL ,
          `title` VARCHAR(255) NULL ,
          `link1` VARCHAR(255) NULL ,
          `link2` VARCHAR(255) NULL ,
          `link3` VARCHAR(255) NULL ,
          `meta` TEXT NULL ,
          `created_at` DATETIME NULL ,
          `updated_at` DATETIME NULL ,
          PRIMARY KEY (`id`) )
        ENGINE = InnoDB;")->execute();
  }

  public function down() {
    \DBUtil::drop_table('episodelists');
  }

}
?>
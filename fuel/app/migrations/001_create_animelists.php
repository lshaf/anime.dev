<?php

namespace Fuel\Migrations;

class Create_animelists {

  public function up() {
    \DB::query("CREATE  TABLE IF NOT EXISTS `animelists` (
          `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
          `name` VARCHAR(255) NULL ,
          `user_id` INT UNSIGNED NOT NULL,
          `pic` VARCHAR(255) NULL ,
          `genre` TEXT NULL ,
          `detail` TEXT NULL ,
          `meta` TEXT NULL ,
          `created_at` DATETIME NULL ,
          `updated_at` DATETIME NULL ,
          PRIMARY KEY (`id`) )
        ENGINE = InnoDB;")->execute();
  }

  public function down() {
    \DBUtil::drop_table('animelists');
  }
}
?>
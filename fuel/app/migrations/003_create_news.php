<?php

namespace Fuel\Migrations;

class Create_news {

  public function up() {
    \DB::query("CREATE  TABLE IF NOT EXISTS `news` (
          `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
          `user_id` INT UNSIGNED NOT NULL ,
          `title` VARCHAR(255) NULL ,
          `pic` VARCHAR(255) NULL ,
          `detail` TEXT NULL ,
          `meta` TEXT NULL ,
          `created_at` DATETIME NULL ,
          `updated_at` DATETIME NULL ,
          PRIMARY KEY (`id`) )
        ENGINE = InnoDB;")->execute();
  }

  public function down() {
    \DBUtil::drop_table('news');
  }

}
?>
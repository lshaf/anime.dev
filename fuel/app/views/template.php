<!DOCTYPE HTML>
<html>
	<head>
		<title>Anime Lengkap<?php echo isset($title) ? " | ".$title : "" ?></title>
		<?php
			echo Asset::css('modern.css').
				 Asset::css('addon.css').
				 Asset::js('jquery.min.js');
		?>
		<link rel="icon" href="<?php echo Asset::get_file('icon.png', 'img') ?>">
	</head>
	<body class="metrouicss">
		<div class="page" id="header">
			<div class="nav-bar">
				<div class="nav-bar-inner padding10">
					<i class="icon-puzzle element brand"></i>
					<a href="<?php echo Uri::base() ?>" class="element brand"> Anime Lengkap</a>
				</div>
			</div>
		</div>
		<div class="page">
			<div class="page-region">
				<div class="page-region-content">
					<div class="grid" style="min-height:570px">
						<div class="row">
							<?php if(Session::get_flash('success')): ?>
							<div class="span12 bg-color-green padding10 fg-color-white">
								<?php echo Session::get_flash('success') ?>
							</div>
							<?php endif; ?>
							<?php if(Session::get_flash('error')): ?>
							<div class="span12 bg-color-redLight padding10 fg-color-white">
								<?php 
									if(is_array(Session::get_flash('error')))
										echo implode("<br>",Session::get_flash('error'));
									else
										echo Session::get_flash('error');
								?>
							</div>
							<?php endif; ?>
						</div>
						<?php echo $body ?>
					</div>
				</div>
			</div>
		</div>
		<div class="page" style="border-top:1px solid #000;padding:10px 5px;">
			&copy; CopyRight by L Shaf
		</div>
		<?php
			echo Asset::js('input-control.js').
				 Asset::js('dropdown.js');
		?>
	</body>
</html>
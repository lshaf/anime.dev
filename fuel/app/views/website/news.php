<div class="row">
	<div class="span8">
		<h2><?php echo $news->title ?></h2>
		<i>Posted By : <?php echo $user[$news->user->username]['fullname']." at ".date('d - F - Y', strtotime($news->created_at)) ?></i><br><hr>
		<div class="img-container"><img src="<?php echo $news->pic ?>" alt="Picture"></div>
		<?php echo nl2br($news->detail); ?>
	</div>
	<div class="span4">
		<?php echo $sidebar ?>
	</div>
</div>
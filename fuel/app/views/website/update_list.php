<div class="row">
	<div class="span8">
		<h2>All Update List</h2>
		Untuk mencari update yang anda cari, silahkan tekan tombol CTRL + F dan masukkan nama anime yang anda cari : <br>
		nb : perlu di ingat, pada halaman ini hanya menampung 30 update terbaru saja.<br><br>
		<div class="row">
			<?php foreach($updates as $episode): ?>
			<a class="span8 bg-color-blue font-white" href="<?php echo Uri::create('anime/detail/'.$episode->anime->id) ?>" 
				title="<?php echo $episode->anime->name.' '.$episode->title ?>">
				<div class="body">
					<?php echo $episode->anime->name.' '.$episode->title ?>
				</div>
			</a>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="span4">
		<?php echo $sidebar ?>
	</div>
</div>
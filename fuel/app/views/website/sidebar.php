<!-- sidebar 0 -->
<div class="row">
	<a class="span2 height2 bg-color-red font-white" href="<?php echo Uri::create('page/problem') ?>">
		<div class="body">
			<div class="cont">
				<i class="icon-bug huge-icon"></i>
			</div>
			<div class="title">Broken Link?</div>
		</div>
	</a>
	<a class="span2 height2 bg-color-pink font-white" href="<?php echo Uri::create('page/book') ?>">
		<div class="body">
			<div class="cont">
				<i class="icon-book huge-icon"></i>
			</div>
			<div class="title">Guest Book</div>
		</div>
	</a>
</div>
<!-- sidebar 1 -->
<div class="row">
	<div class="span4 bg-color-teal font-white">
		<div class="body">
			<h3>New Update</h3>
			<?php foreach($episodes as $episode): ?>
			<a href="<?php echo Uri::create('anime/detail/'.$episode->anime->id) ?>" 
				title="<?php echo $episode->anime->name.' '.$episode->title ?>">
				<?php echo $episode->anime->name.' '.$episode->title ?>
			</a>
			<?php endforeach; ?>
			<a href="<?php echo Uri::create('page/update_list') ?>" title="All Episode">
				All Episode
			</a>
		</div>
	</div>
</div>
<!-- sidebar 2 -->
<div class="row">
	<div class="span4 bg-color-blue font-white">
		<div class="body">
			<h3>New Anime</h3>
			<?php foreach($animes as $anime): ?>
			<a href="<?php echo Uri::create('anime/detail/'.$anime->id) ?>" title="<?php echo $anime->name ?>">
				<?php echo $anime->name ?>
			</a>
			<?php endforeach; ?>
			<a href="<?php echo Uri::create('page/anime_list') ?>" title="All Anime">
				All Anime
			</a>
		</div>
	</div>
</div>
<!-- sidebar 3 -->
<div class="row">
	<div class="span4 bg-color-green font-white">
		<div class="body">
			<h3>Genre</h3>
			<?php foreach($genres as $genre): ?>
			<a href="<?php echo Uri::create('anime/genre/'.$genre) ?>" title="<?php echo $genre ?>">
				<?php echo $genre ?>
			</a>
			<?php endforeach; ?>
			<a href="<?php echo Uri::create('page/genre_list') ?>" title="All Genre">
				All Genre
			</a>
		</div>
	</div>
</div>
<!-- sidebar 3 -->
<div class="row">
	<div class="span4 bg-color-redLight font-white">
		<div class="body">
			<h3>News</h3>
			<?php foreach($news as $data): ?>
			<a href="<?php echo Uri::create('anime/news/'.$data->id) ?>" title="<?php echo $data->title ?>">
				<?php echo $data->title ?>
			</a>
			<?php endforeach; ?>
			<a href="<?php echo Uri::create('page/news_list') ?>" title="All News">
				All News
			</a>
		</div>
	</div>
</div>
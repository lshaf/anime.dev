<?php $no = 1; ?>
<div class="row">
	<div class="span8">
		<h2>All Genre List</h2>
		Untuk mencari genre yang anda cari, silahkan tekan tombol CTRL + F dan masukkan nama genre yang anda cari : <br><br>
		<div class="row">
			<?php foreach($genres as $genre): ?>
			<a class="span4 bg-color-blue font-white" href="<?php echo Uri::create('anime/genre/'.$genre) ?>" 
				title="<?php echo $genre ?>">
				<div class="body">
					<?php echo $genre ?>
				</div>
			</a>
			<?php
			if($no % 2 == 0 and $no != count($genres)){
				echo '
				</div>
				<div class="row">';
			}
			$no++;
			?>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="span4">
		<?php echo $sidebar ?>
	</div>
</div>
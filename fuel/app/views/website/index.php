<div class="row">
	<div class="span7 height4 bg-color-teal font-white hovered">
		<div class="body">
			<div class="cont" style="height:247px;">
				<?php foreach($updatelists as $update): ?>
				<a href="<?php echo Uri::create('anime/detail/'.$update->anime->id.'/') ?>"
				title="<?php echo $update->anime->name.' '.$update->title ?>">
					<?php echo $update->anime->name.' '.$update->title ?>
				</a>
				<?php endforeach; ?>
			</div>
			<div class="title">New Anime Update / Last Update</div>
		</div>
	</div>
	<div class="span5 height2 bg-color-yellow font-white hovered">
		<div class="body">
			<div class="cont">
				<?php foreach($animelists as $anime): ?>
				<a href="<?php echo Uri::create('anime/detail/'.$anime->id.'/') ?>" title="<?php echo $anime->name ?>">
					<?php echo $anime->name ?>
				</a>
				<?php endforeach; ?>
			</div>
			<div class="title">New Anime</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="span5 height2 bg-color-purple font-white hovered">
		<div class="body">
			<div class="cont">
				<?php foreach($news as $data): ?>
				<a href="<?php echo Uri::create('anime/news/'.$data->id) ?>" title="<?php echo $data->title ?>">
					<?php echo $data->title ?>
				</a>
				<?php endforeach; ?>
			</div>
			<div class="title">All News</div>
		</div>
	</div>
</div>
<div class="row">
	<a class="span3 height2 bg-color-blue font-white hovered" href="#">
		<div class="body">
			<div class="cont">
				<i class="icon-facebook huge-icon"></i>
			</div>
			<div class="title">Facebook</div>
		</div>
	</a>
	<a class="span3 height2 bg-color-teal font-white hovered" href="#">
		<div class="body">
			<div class="cont">
				<i class="icon-twitter huge-icon"></i>
			</div>
			<div class="title">Twitter</div>
		</div>
	</a>
	<a class="span3 height2 bg-color-orangeDark font-white hovered" href="#">
		<div class="body">
			<div class="cont">
				<i class="icon-google-plus huge-icon"></i>
			</div>
			<div class="title">Google Plus</div>
		</div>
	</a>
	<a class="span3 height2 bg-color-purple font-white hovered" href="#">
		<div class="body">
			<div class="cont">
				<i class="icon-mail huge-icon"></i>
			</div>
			<div class="title">E-Mail</div>
		</div>
	</a>
</div>
<div class="row">
	<a class="span4 height2 bg-color-magenta font-white hovered" href="<?php echo Uri::create('page/problem') ?>">
		<div class="body">
			<div class="cont">
				<i class="icon-bug huge-icon"></i>
			</div>
			<div class="title">Broken Link?</div>
		</div>
	</a>
	<a class="span4 height2 bg-color-red font-white hovered" href="<?php echo Uri::create('page/about') ?>">
		<div class="body">
			<div class="cont">
				<i class="icon-user-3 huge-icon"></i>
			</div>
			<div class="title">About Us</div>
		</div>
	</a>
	<a class="span4 height2 bg-color-pink font-white hovered" href="<?php echo Uri::create('page/book') ?>">
		<div class="body">
			<div class="cont">
				<i class="icon-book huge-icon"></i>
			</div>
			<div class="title">Guest Book</div>
		</div>
	</a>
</div>
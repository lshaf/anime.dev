<div class="row">
	<div class="span8">
		<h2><?php echo $title ?></h2>
		<div class="row">
			<?php foreach($animes as $anime): ?>
			<a class="span8 bg-color-blue font-white" href="<?php echo Uri::create('anime/detail/'.$anime['id']) ?>" 
				title="<?php echo $anime['name'] ?>">
				<div class="body">
					<?php echo $anime['name'] ?>
				</div>
			</a>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="span4">
		<?php echo $sidebar ?>
	</div>
</div>
<div class="row">
	<div class="span8">
		<h2><?php echo $anime->name ?></h2>
		<i>Posted By : <?php echo $user[$anime->user->username]['fullname']." at ".date('d - F - Y', strtotime($anime->created_at)) ?><br>
		Genre : 
		<?php 
		$data = explode(",", $anime->genre);
		for($i = 0;$i < count($data);$i++){
			echo " ".Html::anchor(Uri::create('anime/genre/'.$data[$i]), $data[$i]);
		}
		?></i><br><hr>
		<div class="img-container"><img src="<?php echo $anime->pic ?>" alt="Picture"></div>
		<?php echo nl2br($anime->detail); ?>
		<hr>
		<?php foreach($anime->episode as $episode): ?>
		<div class="row">
			<div class="span2 bg-color-blue font-white" name="id1">
				<div class="body"><?php echo $episode->title ?></div>
			</div>
			<div class="span2 bg-color-darken font-white">
				<a style="display:block" class="body bg-color-blue" href="<?php echo $episode->link1 ?>" target="_blank">
					<i class="icon-download"></i> Primary Link
				</a>
			</div>
			<div class="span2 bg-color-darken font-white">
				<?php if($episode->link2 != ""): ?>
				<a style="display:block" class="body bg-color-blue" href="<?php echo $episode->link2 ?>" target="_blank">
					<i class="icon-download"></i> Mirror 1
				</a>
				<?php else: ?>
				<div class="body"><i class="icon-download"></i> Mirror 1</div>
				<?php endif; ?>
			</div>
			<div class="span2 bg-color-darken font-white">
				<?php if($episode->link3 != ""): ?>
				<a style="display:block" class="body bg-color-blue" href="<?php echo $episode->link3 ?>" target="_blank">
					<i class="icon-download"></i> Mirror 2
				</a>
				<?php else: ?>
				<div class="body"><i class="icon-download"></i> Mirror 2</div>
				<?php endif; ?>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
	<div class="span4">
		<?php echo $sidebar ?>
	</div>
</div>
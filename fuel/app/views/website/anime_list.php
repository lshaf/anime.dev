<?php $no = 1; ?>
<div class="row">
	<div class="span8">
		<h2>All Anime List</h2>
		Untuk mencari anime yang anda cari, silahkan tekan tombol CTRL + F dan masukkan nama anime yang anda cari : <br><br>
		<div class="row">
			<?php foreach($animes as $anime): ?>
			<a class="span4 bg-color-blue font-white" href="<?php echo Uri::create('anime/detail/'.$anime->id) ?>" 
				title="<?php echo $anime->name ?>">
				<div class="body">
					<?php echo $anime->name ?>
				</div>
			</a>
			<?php
			if($no % 2 == 0 and $no != count($animes)){
				echo '
				</div>
				<div class="row">';
			}
			$no++;
			?>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="span4">
		<?php echo $sidebar ?>
	</div>
</div>
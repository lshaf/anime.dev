<div class="row">
	<div class="span8">
		<h2>All News List</h2>
		<div class="row">
			<?php foreach($news as $data): ?>
			<a class="span8 bg-color-blue font-white" href="<?php echo Uri::create('anime/news/'.$data->id) ?>" 
				title="<?php echo $data->title ?>">
				<div class="body">
					<?php echo $data->title ?>
				</div>
			</a>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="span4">
		<?php echo $sidebar ?>
	</div>
</div>
<div class="row">
	<div class="span12">
		<div class="page secondary with-sidebar">
			<div class="page-sidebar">
				<ul>
					<li>
						<a href="<?php echo Uri::create('admin/') ?>">
							<i class="icon-home"></i> Dashboard
						</a>
					</li>
					<li class="sticker sticker-color-red dropdown" data-role="dropdown">
						<a href="#"><i class="icon-accessibility"></i> Anime</a>
						<ul class="sub-menu sidebar-dropdown-menu light">
							<li><a href="<?php echo Uri::create('admin/anime/') ?>">List</a></li>
							<li><a href="<?php echo Uri::create('admin/anime/create') ?>">Create</a></li>
						</ul>
					</li>
					<li class="sticker sticker-color-purple dropdown" data-role="dropdown">
						<a href="#"><i class="icon-puzzle"></i> Update</a>
						<ul class="sub-menu sidebar-dropdown-menu light">
							<li><a href="<?php echo Uri::create('admin/episode/') ?>">List</a></li>
							<li><a href="<?php echo Uri::create('admin/episode/create') ?>">Create</a></li>
						</ul>
					</li>
					<li class="sticker sticker-color-yellow dropdown" data-role="dropdown">
						<a href="#"><i class="icon-newspaper"></i> News</a>
						<ul class="sub-menu sidebar-dropdown-menu light">
							<li><a href="<?php echo Uri::create('admin/news/') ?>">List</a></li>
							<li><a href="<?php echo Uri::create('admin/news/create') ?>">Create</a></li>
						</ul>
					</li>
					<li class="sticker sticker-color-blue dropdown" data-role="dropdown">
						<a href="#"><i class="icon-user"></i> Users</a>
						<ul class="sub-menu sidebar-dropdown-menu light">
							<li><a href="<?php echo Uri::create('admin/user/') ?>">List</a></li>
							<li><a href="<?php echo Uri::create('admin/user/create/') ?>">Create</a></li>
						</ul>
					</li>
					<li>
						<a href="<?php echo Uri::create('admin/logout') ?>">
							<i class="icon-switch"></i> Logout
						</a>
					</li>
				</ul>
			</div>
			<div class="page-region">
				<div class="page-region-content">
					<div class="span9">
						<?php echo $admin_body ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<h1>News List</h1>
<table class="striped hovered bordered">
	<thead>
		<tr>
			<th>Title</th>
			<th>Posted By</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($news):
		foreach($news as $data): ?>
		<tr>
			<td><?php echo $data->title ?></td>
			<td><?php echo $user[$data->user->username]['fullname'] ?></td>
			<td class="center">
				<a href="<?php echo Uri::create('admin/news/edit/'.$data->id) ?>" title="Edit">
					<i class="icon-pencil"></i>
				</a>
				<a onclick="return confirm('Are you sure?')" href="<?php echo Uri::create('admin/news/delete/'.$data->id) ?>" title="Delete">
					<i class="icon-cancel-2"></i>
				</a>
			</td>
		</tr>
		<?php 
		endforeach; 
	else: ?>
		<tr>
			<td colspan=3 class='center'>No List</td>
		</tr>
	<?php endif;?>
	</tbody>
</table>
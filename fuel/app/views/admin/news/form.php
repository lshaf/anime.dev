<?php echo Form::open(array('enctype' => 'multipart/form-data')); ?>
	<div class="input-control text">
		<input type="text" name="title" placeholder="Title" value="<?php echo Input::post('title', isset($news) ? $news->title : ''); ?>"/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input type="file" name="pic" placeholder="Picture"/>
	</div>
	<div class="input-control textarea">
		<textarea name="detail" placeholder="Detail" rows="22" style="resize:none;"><?php echo Input::post('detail', isset($news) ? $news->detail : ''); ?></textarea>
	</div>
	<input type="submit" value="Save" class="btn btn-primary">
	<input type="button" onclick="window.location='<?php echo Uri::create('admin/news/') ?>'" class="btn" value="back">
<?php echo Form::close(); ?>
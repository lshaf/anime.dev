<div class="row">
	<div class="span12">
		<form action="<?php echo Uri::create('admin/login/check') ?>" method="POST">
			<div class="input-control text">
				<input type="text" name="email" placeholder="Username or Email"/>
				<button class="btn-clear"></button>
			</div>
			<div class="input-control text">
				<input type="password" name="pass" placeholder="Password"/>
				<button class="btn-clear"></button>
			</div>
			<input type="submit" value="Login"/>
			<input type="reset"  value="Reset"/>
		</form>
	</div>
</div>
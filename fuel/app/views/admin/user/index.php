<h1>User List</h1>
<table class="striped hovered bordered">
	<thead>
		<tr>
			<th>Username</th>
			<th>Name</th>
			<th>Email</th>
			<th>Group</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($users as $user): ?>
		<tr>
			<td><?php echo $user->username ?></td>
			<td><?php echo $profile[$user->username]['fullname'] ?></td>
			<td><?php echo $user->email ?></td>
			<td class="center"><?php echo $user->group ?></td>
			<td class="center">
				<a href="<?php echo Uri::create('admin/user/edit/'.$user->id) ?>" title="Edit">
					<i class="icon-pencil"></i>
				</a>
				<a onclick="return confirm('Are you sure?')" href="<?php echo Uri::create('admin/user/delete/'.$user->id) ?>" title="Delete">
					<i class="icon-cancel-2"></i>
				</a>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
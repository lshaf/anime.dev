<?php echo Form::open(array("class"=>"form-horizontal")); ?>
	<div class="input-control text">
		<input type="text" name="username" placeholder="Username" value="<?php echo Input::post('username', isset($user) ? $user->username : ''); ?>" <?php echo (isset($user) ? 'disabled' : '') ?>/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input type="password" name="password" placeholder="Password" value="<?php echo Input::post('password', '') ?>"/>
		<button class="btn-clear"></button>
	</div>
	<?php if(isset($user)): ?>
	<div class="input-control text">
		<input type="password" name="cpassword" placeholder="Old Password" value="<?php echo Input::post('cpassword', '') ?>"/>
		<button class="btn-clear"></button>
	</div>
	<?php endif; ?>
	<div class="input-control text">
		<input type="text" name="name" placeholder="Fullname" value="<?php echo Input::post('name', isset($profile['fullname']) ? $profile['fullname'] : ''); ?>"/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input type="text" name="email" placeholder="Email" value="<?php echo Input::post('email', isset($user) ? $user->email : ''); ?>"/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control select">
		<?php echo Form::select('group', Input::post('group', isset($user) ? $user->group : '2'), $groups);	?>
	</div>
	<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>
	<input type="button" onclick="window.location='<?php echo Uri::create('admin/user/') ?>'" class="btn" value="back">
<?php echo Form::close(); ?>
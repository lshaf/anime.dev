<?php echo Asset::css('chosen.css').Asset::js('jquery.chosen.js'); ?>
<?php echo Form::open(); ?>
	<div class="input-control select">
		<select id="select" data-placeholder="Select Anime Title" name="anime_id" required>
			<option value=""></option>
			<?php foreach ($animelists as $animelist): ?>
		    	<option <?php echo (Input::post('anime_id', isset($episode) ? $episode->anime_id : '0') == $animelist->id) ? 'selected' : '' ?>
		    		value="<?php echo $animelist->id ?>">
		    		<?php echo $animelist->name; ?>
		    	</option> 
			<?php endforeach; ?>
		</select>
	</div>
	<div class="input-control text">
		<input type="text" name="title" placeholder="Title" value="<?php echo Input::post('title', isset($episode) ? $episode->title : ''); ?>" required/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input type="text" name="link1" placeholder="Link 1" value="<?php echo Input::post('link1', isset($episode) ? $episode->link1 : ''); ?>" required/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input type="text" name="link2" placeholder="Link 2" value="<?php echo Input::post('link2', isset($episode) ? $episode->link2 : ''); ?>"/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input type="text" name="link3" placeholder="Link 3" value="<?php echo Input::post('link3', isset($episode) ? $episode->link3 : ''); ?>"/>
		<button class="btn-clear"></button>
	</div>
	<input type="submit" value="Save" class="btn btn-primary">
	<input type="button" onclick="window.location='<?php echo Uri::create('admin/episode/') ?>'" class="btn" value="back">
<?php echo Form::close(); ?>
<script type="text/javascript">
	$("#select").chosen();
</script>
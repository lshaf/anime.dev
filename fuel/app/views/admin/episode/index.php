<?php echo Asset::css('chosen.css').Asset::js('jquery.chosen.js'); ?>
<h1>Episode List</h1>
<div class="input-control select">
	<select id="select" data-placeholder="Select Anime Title" tabindex="<?php echo Input::get('anime', '1') ?>"
		onchange="window.location='<?php echo Uri::create('admin/episode/') ?>?anime='+$('#select').val();">
		<option value=""></option>
		<?php if ($animelists): ?>
			<?php foreach ($animelists as $animelist): ?>
		    	<option <?php echo (Input::get('anime', '0') == $animelist->id) ? 'selected' : '' ?>
		    		value="<?php echo $animelist->id ?>">
		    		<?php echo $animelist->name; ?>
		    	</option> 
			<?php endforeach; ?>
		<?php endif; ?>
	</select>
</div>
<table class="striped hovered bordered">
	<thead>
		<tr>
			<th>Title</th>
			<th>Anime</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($episodes):
		foreach($episodes as $episode): ?>
		<tr>
			<td><?php echo $episode->title ?></td>
			<td><?php echo $episode->anime->name ?></td>
			<td class="center">
				<a href="<?php echo Uri::create('admin/episode/edit/'.$episode->id) ?>" title="Edit">
					<i class="icon-pencil"></i>
				</a>
				<a onclick="return confirm('Are you sure?')" href="<?php echo Uri::create('admin/episode/delete/'.$episode->id) ?>" title="Delete">
					<i class="icon-cancel-2"></i>
				</a>
			</td>
		</tr>
		<?php 
		endforeach; 
	else: ?>
		<tr>
			<td colspan=3 class='center'>No List</td>
		</tr>
	<?php endif;?>
	</tbody>
</table>
<script type="text/javascript">
	$("#select").chosen();
</script>
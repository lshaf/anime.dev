<h1>Anime List</h1>
<table class="striped hovered bordered">
	<thead>
		<tr>
			<th>Title</th>
			<th>Genre</th>
			<th>Posted By</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($animelists):
		foreach($animelists as $anime): ?>
		<tr>
			<td><?php echo $anime->name ?></td>
			<td><?php echo $anime->genre ?></td>
			<td><?php echo $user[$anime->user->username]['fullname'] ?></td>
			<td class="center">
				<a href="<?php echo Uri::create('admin/anime/edit/'.$anime->id) ?>" title="Edit">
					<i class="icon-pencil"></i>
				</a>
				<a onclick="return confirm('Are you sure?')" href="<?php echo Uri::create('admin/anime/delete/'.$anime->id) ?>" title="Delete">
					<i class="icon-cancel-2"></i>
				</a>
			</td>
		</tr>
		<?php 
		endforeach; 
	else: ?>
		<tr>
			<td colspan=4 class='center'>No List</td>
		</tr>
	<?php endif;?>
	</tbody>
</table>
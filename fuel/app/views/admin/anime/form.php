<?php echo Asset::css('tags.css').Asset::js('tags.js'); ?>
<?php echo Form::open(array('enctype' => 'multipart/form-data')); ?>
	<div class="input-control text">
		<input type="text" name="name" placeholder="Title" value="<?php echo Input::post('name', isset($anime) ? $anime->name : ''); ?>"/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input id="genre" type="text" name="genre" placeholder="Genre" value="<?php echo Input::post('genre', isset($anime) ? $anime->genre : ''); ?>"/>
		<button class="btn-clear"></button>
	</div>
	<div class="input-control text">
		<input type="file" name="pic" placeholder="Picture"/>
	</div>
	<div class="input-control textarea">
		<textarea name="detail" placeholder="Detail" rows="17" style="resize:none;"><?php echo Input::post('detail', isset($anime) ? $anime->detail : ''); ?></textarea>
	</div>
	<input type="submit" value="Save" class="btn btn-primary">
	<input type="button" onclick="window.location='<?php echo Uri::create('admin/anime/') ?>'" class="btn" value="back">
<?php echo Form::close(); ?>
<script type="text/javascript">
	$('#genre').tagsInput({
		width: '100%',
	});
</script>